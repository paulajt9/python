def main():
    n = int(input("Enter a number to calculate a serie: "))
    calculate_serie(n)

def calculate_serie(n):
    result=0
    for num in range (n+1):
        result = (num+1)/n + result
        n = n-1
        if (n == 0):
            break
    print(result)

main()


def main():
    n =int(input('Enter a number to create a shape: '))
    print_pattern(n)


def print_pattern(n):
    while (n > 0):
        for num in range(n):
            print("*", end = " ")
        print()
        n= n-1

main()

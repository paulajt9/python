
class Car:

    def __init__(self, year, make):
        self.year_model= year
        self.make = make
        self.speed = 0

    def accelerate(self, speed_step):
        self.speed= self.speed + speed_step


    def brake(self, reduce_speed_step):
        self.speed = self.speed - reduce_speed_step


    def get_speed(self):
        return (print("The speed of the car now is: " + str(self.speed) + "Km/h"))


NewCar = Car(1997, "Toyota")
NewCar.accelerate(20)
NewCar.get_speed()
NewCar.accelerate(10)
NewCar.get_speed()
NewCar.accelerate(20)
NewCar.get_speed()
NewCar.accelerate(10)
NewCar.get_speed()
NewCar.accelerate(10)
NewCar.get_speed()
NewCar.brake(10)
NewCar.get_speed()
NewCar.brake(10)
NewCar.get_speed()
NewCar.brake(30)
NewCar.get_speed()
NewCar.brake(0)
NewCar.get_speed()
NewCar.brake(5)
NewCar.get_speed()

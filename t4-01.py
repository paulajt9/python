
import math

class Circle:

    def __init__(self, radius, x, y):
        self.r = radius
        self.x = x
        self.y = y

    def area(self):
        area= math.pi*self.r*self.r
        return str(area)

    def circumference(self):
        length= 2*math.pi*self.r
        return str(length)

    def get_top(self):
        top= self.y+self.r
        return str(top)

    def get_right(self):
        left= self.x-self.r
        return str(left)

NewCircle = Circle(8, 0, 0)
print("The area of the Circle is " + NewCircle.area())
print("The lenght of the Circle is " + NewCircle.circumference())
print("The top point of the Circle is " + NewCircle.get_top())
print("The right point of the Circle is " + NewCircle.get_right())

import random
def main():
    n= int(input('Enter a number:'))
    toss_coin(n)


def toss_coin(n):
    count0=0
    count1=0
    for i in range(n):
        num= random.randint(0,1)
        if (num == 0):
            count0= count0+1
        else:
            count1= count1+1
    print('The number of 0 in the sequence is:', count0, 'The number of 1 in the sequence is', count1)

main()

